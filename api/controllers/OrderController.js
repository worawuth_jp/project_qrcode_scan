const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const path = require("path");
const OrderService = require("../services/OrderService");
const dayjs = require("dayjs");

class OrderController{
    async listOrders(){
        try {
            const resultSuccess = await OrderService.listOrders();
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.listOrders() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listFoodOrders(){
        try {
            const resultSuccess = await OrderService.listFoodOrders();
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.listFoodOrders() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listOrderById(params){
        try {
            const isUndefined = (params.order_id || params.table_id) && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }

            let resultSuccess;
            if(params.table_id){
                console.log("table");
                let notPay=null;
                if(params.not_pay !== void 0){
                    notPay = true;
                }
                resultSuccess = await OrderService.listOrderByTable(params.table_id,notPay);
            }

            if(params.order_id){
                resultSuccess = await OrderService.listOrderById(params.order_id);
            }
            
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.listOrderById() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addOrder(body){
        try {
            const isUndefined = body.table_id && body.member_id && body.foods && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            
            const tableId = body.table_id;
            const memberId = body.member_id;
            const foods = body.foods;
            const orderCode = `ORDER${memberId}${tableId}${dayjs().unix()}`;
            const resultSuccess = await OrderService.addOrder(orderCode,tableId,memberId,foods);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.addOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editOrder(body){
        try {
            const isUndefined = body.order_id && body.order_code && body.status && body.foods && body.table_id && body.member_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const orderId = body.order_id;
            const orderCode = body.order_code;
            const status = body.status;
            const foods = body.foods;
            const tableId = body.table_id;
            const memberId = body.member_id;
            const resultSuccess = await OrderService.editOrder(orderId,orderCode,tableId,memberId,status,foods);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.editOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editOrderStatus(body){
        try {
            const isUndefined = body.order_id && body.status && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const orderId = body.order_id;
            const status = body.status;
            const pay = body.pay;
            const resultSuccess = await OrderService.editOrderStatus(orderId,status,pay);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.editOrderStatus() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editOrderFood(body){
        try {
            const isUndefined = body.order_food_id && body.status !== void 0 && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const orderFoodId = body.order_food_id;
            const status = body.status;
            const resultSuccess = await OrderService.editFoodOrder(orderFoodId,status);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.editOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteOrder(body){
        try {
            const isUndefined = body.order_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const orderId = body.order_id;
            
            const resultSuccess = await OrderService.deleteOrder(orderId);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.deleteOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

}

module.exports = new OrderController()