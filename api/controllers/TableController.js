const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const TableService = require("../services/TableService");

class TableController{
    async listTables(){
        try {
            const resultSuccess = await TableService.listTables();
            return resultSuccess;
        } catch (error) {
            console.error('TableController.listTables() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listTablesById(params){
        try {
            const isUndefined = params.table_id;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await TableService.listTableById(params.table_id);
            return resultSuccess;
        } catch (error) {
            console.error('TableController.listTables() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addTable(body){
        try {
            const isUndefined = body.table_no;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await TableService.addTable(body.table_no);
            return resultSuccess;
        } catch (error) {
            console.error('TableController.addTable() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editTable(body){
        try {
            const isUndefined = body.table_no && body.table_id && body.isShow;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await TableService.editTable(body.table_id,body.table_no,body.isShow);
            return resultSuccess;
        } catch (error) {
            console.error('TableController.editTable() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteTable(body){
        try {
            const isUndefined = body.table_id;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await TableService.deleteTable(body.table_id);
            return resultSuccess;
        } catch (error) {
            console.error('TableController.deleteTable() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }
}

module.exports = new TableController()