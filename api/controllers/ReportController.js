const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const path = require("path");
const dayjs = require("dayjs");
const ReportService = require("../services/ReportService");
const TransformModelUtils = require("../utils/TransformModelUtils");

class ReportController{
    async listReportPay(body){
        try {
            const resultSuccess = await ReportService.listReportPay(body.start_date,body.end_date);
            return await TransformModelUtils.transformResponseWithDataModel(resultSuccess);
        } catch (error) {
            console.error('ReportController.listOrders() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listReportOrder(body){
        try {
            const resultSuccess = await ReportService.listReportOrder(body.start_date,body.end_date);
            return await TransformModelUtils.transformResponseWithDataModel(resultSuccess);
        } catch (error) {
            console.error('ReportController.listReportOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async sendEmail(body){
        try {
            if(!body.email || body.email == ''){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await ReportService.sendEmail(body.start_date,body.end_date,body.email);
            return resultSuccess;
        } catch (error) {
            console.error('ReportController.sendEmail() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

}

module.exports = new ReportController()