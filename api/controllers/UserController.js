const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const UserService = require("../services/UserService");
const path = require("path");
const http = require("../constants/http");
const message = require("../constants/message");

class UserController{
    async listUsers(params){
        try {
            let id = undefined;
            if(params){
                id = params.id;
            }
            const resultSuccess = await UserService.getUsers(id)
            return resultSuccess;
        } catch (error) {
            console.error('UserController.listUsers() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addUser(body,file){
        try {
            const isUndefined = body.firstname && body.lastname && body.username && body.password && body.role_id
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const firstname = body.firstname;
            const lastname = body.lastname;
            const username = body.username;
            const password = body.password;
            const role_id = body.role_id;
            let tempPath,imgPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/members/member_${random}_${role_id}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            
            const resultSuccess = await UserService.addUser(firstname,lastname,username,password,role_id,tempPath,targetPath,imgPath);
            return resultSuccess;
        } catch (error) {
            console.error('UserController.addUser() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editUser(body,file){
        try {
            const firstname = body.firstname;
            const lastname = body.lastname;
            const username = body.username;
            const password = body.password;
            const role_id = body.role_id;
            const member_id = body.member_id;
            let tempPath,imgPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/members/member_${random}_${role_id}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            const resultSuccess = await UserService.editUser(member_id,firstname,lastname,username,password,role_id,tempPath,targetPath,imgPath);
            return resultSuccess;
        } catch (error) {
            console.error('UserController.editUser() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteUser(body){
        try {
            const member_id = body.member_id;
            const resultSuccess = await UserService.deleteUser(member_id);
            return resultSuccess;
        } catch (error) {
            console.error('UserController.deleteUser() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }
}

module.exports = new UserController()