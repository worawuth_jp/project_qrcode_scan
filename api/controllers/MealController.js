const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const path = require("path");
const MealService = require("../services/MealService");

class MealController{
    async listMeals(){
        try {
            const resultSuccess = await MealService.listMeals();
            return resultSuccess;
        } catch (error) {
            console.error('MealController.listMeals() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listMealsById(params){
        try {
            const isUndefined =( params.meal_id || params.not_in) && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }

            let notInFoodId = null;
            if(params.not_in !== void 0 && 1){
                notInFoodId = params.not_in;
            }
            let resultSuccess;
            if(notInFoodId){
                resultSuccess = await MealService.listMealNotInFoodId(notInFoodId);
            }else{
                resultSuccess = await MealService.listMealById(params.meal_id);
            }
            
            return resultSuccess;
        } catch (error) {
            console.error('MealController.listMealsById() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addMeal(body){
        try {
            const isUndefined = body.material_name && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const materialName = body.material_name;
            const resultSuccess = await MealService.addMeal(materialName);
            return resultSuccess;
        } catch (error) {
            console.error('MealController.addMeal() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editMeal(body){
        try {
            const isUndefined = body.material_name && body.isEnable !== void 0 && body.material_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const materialName = body.material_name;
            const isEnable = body.isEnable;
            const materialId = body.material_id
            
            const resultSuccess = await MealService.editMeal(materialId,materialName,isEnable);
            return resultSuccess;
        } catch (error) {
            console.error('MealController.editMeal() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteMeal(body){
        try {
            const isUndefined = body.material_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const materialId = body.material_id;
            
            const resultSuccess = await MealService.deleteMeal(materialId);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.deleteOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addMealFood(body){
        try {
            const isUndefined = body.data && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const data = body.data;
            const resultSuccess = await MealService.addMealFood(data);
            return resultSuccess;
        } catch (error) {
            console.error('MealController.addMealFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async updateMealFood(body){
        try {
            const isUndefined = body.food_material_id && body.food_material_price && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodMaterialId = body.food_material_id;
            const foodMaterialPrice = body.food_material_price;
            const resultSuccess = await MealService.updateMealFood(foodMaterialId,foodMaterialPrice);
            return resultSuccess;
        } catch (error) {
            console.error('MealController.updateMealFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteMealFood(body){
        try {
            const isUndefined = body.food_material_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const id = body.food_material_id;
            const resultSuccess = await MealService.deleteFoodMaterial(id);
            return resultSuccess;
        } catch (error) {
            console.error('MealController.deleteMealFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

}

module.exports = new MealController()