var mysql = require("mysql");

class DBAPI {
  #_con;
  constructor(){
    this.getConnection()
  }
  async getConnection() {
    try {
      this._con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "booking_qr",
      });

      this._con.connect(function (err) {
        if (err) throw err;
        console.log("Connected!");
      });

      return this._con;
    } catch (error) {
      console.error("DBAPI EXCEPTION : ", error);
      throw error;
    }
  }

  async query(sql, params) {
    return await new Promise((resolve, reject) => {
      this._con.query(sql, params, (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
  }

  closeConnection() {
    console.log("Closed Connection");
    this._con.commit();
    this._con.end();
  }
}

module.exports = DBAPI;
