const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");
const fs = require('fs');
const { COMING_STATUS, NOT_PAY_STATUS, PAY_STATUS, CANCEL_STATUS, COMING_STATUS_NUM, NOT_PAY_STATUS_NUM, PAY_STATUS_NUM, CANCEL_STATUS_NUM } = require("../constants/status");

class OrderService{
    async listOrders(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT orders.order_id,orders.order_code,orders.table_id,tables.table_no,orders.member_id,orders.status,orders.created_at 
            FROM orders 
            INNER JOIN tables ON tables.table_id = orders.table_id`;
            const resultSet = await DBApi.query(sql)
            for await (const data of resultSet){
                let query = `SELECT 
                ORDERFOOD.order_food_id,
                ORDERFOOD.food_id,
                ORDERFOOD.order_num,
                ORDERFOOD.order_amount,
                ORDERFOOD.order_status,
                order_food_materials.food_material_price,
                FOOD.food_name,
                FOOD.food_price,
                FOOD.food_detail,
                FOOD.food_img,
                FOOD.isEnable
                FROM order_foods ORDERFOOD
                INNER JOIN foods FOOD ON FOOD.food_id = ORDERFOOD.food_id
                INNER JOIN order_food_materials ON order_food_materials.order_food_id = ORDERFOOD.order_food_id
                INNER JOIN food_materials MATERIAL ON MATERIAL.food_material_id = order_food_materials.food_material_id 
                WHERE ORDERFOOD.order_id=?`
                let params = [data.order_id];
                data.order_foods = await DBApi.query(query,params);
                for await (const item of data.order_foods){
                    let params2 = [];
                    const query2 = `SELECT order_food_materials.food_material_price,order_food_materials.food_material_id,materials.material_name FROM order_food_materials
                    INNER JOIN food_materials on food_materials.food_material_id = order_food_materials.food_material_id
                    INNER JOIN materials on materials.material_id = food_materials.material_id
                    WHERE order_food_materials.order_food_id=? `;
                    params2.push(item.order_food_id);
                    item.food_materials = await DBApi.query(query2,params2);
                }
                
                
                data.isEnable = data.status == 3 ? false : true;
                data.status = data.status == 0 ? COMING_STATUS : data.status == 1 ? NOT_PAY_STATUS : data.status == 2 ? PAY_STATUS : data.status == 3 ? CANCEL_STATUS :'';
            }
            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
        } catch (error) {
            console.error("OrderService.listOrders() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listOrderById(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT orders.order_id,orders.order_code,orders.table_id,tables.table_no,orders.member_id,orders.status,orders.created_at 
            FROM orders 
            INNER JOIN tables ON tables.table_id = orders.table_id
            WHERE order_id=?`;
            let queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {};
            for await (const data of resultSet){
                let query = `SELECT 
                ORDERFOOD.order_food_id,
                ORDERFOOD.food_id,
                ORDERFOOD.order_num,
                ORDERFOOD.order_amount,
                ORDERFOOD.order_status,
                order_food_materials.food_material_price,
                FOOD.food_name,
                FOOD.food_price,
                FOOD.food_detail,
                FOOD.food_img,
                FOOD.isEnable
                FROM order_foods ORDERFOOD
                INNER JOIN foods FOOD ON FOOD.food_id = ORDERFOOD.food_id
                INNER JOIN order_food_materials ON order_food_materials.order_food_id = ORDERFOOD.order_food_id
                INNER JOIN food_materials MATERIAL ON MATERIAL.food_material_id = order_food_materials.food_material_id 
                WHERE ORDERFOOD.order_id=?`
                let params = [data.order_id];
                data.order_foods = await DBApi.query(query,params);
                for await (const item of data.order_foods){
                    let params2 = [];
                    const query2 = `SELECT order_food_materials.food_material_price,order_food_materials.food_material_id,materials.material_name FROM order_food_materials
                    INNER JOIN food_materials on food_materials.food_material_id = order_food_materials.food_material_id
                    INNER JOIN materials on materials.material_id = food_materials.material_id
                    WHERE order_food_materials.order_food_id=? `;
                    params2.push(item.order_food_id);
                    item.food_materials = await DBApi.query(query2,params2);
                }
                data.isEnable = data.status == 3 ? false : true;
                data.status = data.status == 0 ? COMING_STATUS : data.status == 1 ? NOT_PAY_STATUS : data.status == 2 ? PAY_STATUS : data.status == 3 ? CANCEL_STATUS :'';
                result = data;
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listOrderById() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listOrderByTable(id,pay=null){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT orders.order_id,orders.order_code,orders.table_id,tables.table_no,orders.member_id,orders.status,orders.created_at 
            FROM orders 
            INNER JOIN tables ON tables.table_id = orders.table_id
            WHERE tables.table_id=?`;
            let queryParams = [id];
            if(pay !== null){
                sql += ` AND orders.status not in (2,3)`;
            }
            sql += ` ORDER BY orders.order_id DESC`;
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = [];
            for await (const data of resultSet){
                let query = `SELECT 
                ORDERFOOD.order_food_id,
                ORDERFOOD.food_id,
                ORDERFOOD.order_num,
                ORDERFOOD.order_amount,
                ORDERFOOD.order_status,
                FOOD.food_name,
                FOOD.food_price,
                FOOD.food_detail,
                FOOD.food_img,
                FOOD.isEnable
                FROM order_foods ORDERFOOD
                INNER JOIN foods FOOD ON FOOD.food_id = ORDERFOOD.food_id
                WHERE ORDERFOOD.order_id=?`
                let params = [data.order_id];
                data.order_foods = await DBApi.query(query,params);
                for await (const item of data.order_foods){
                    let params2 = [];
                    const query2 = `SELECT order_food_materials.food_material_price,order_food_materials.food_material_id,materials.material_name FROM order_food_materials
                    INNER JOIN food_materials on food_materials.food_material_id = order_food_materials.food_material_id
                    INNER JOIN materials on materials.material_id = food_materials.material_id
                    WHERE order_food_materials.order_food_id=? `;
                    params2.push(item.order_food_id);
                    item.food_materials = await DBApi.query(query2,params2);
                }
                data.isEnable = data.status == 3 ? false : true;
                data.status = data.status == 0 ? COMING_STATUS : data.status == 1 ? NOT_PAY_STATUS : data.status == 2 ? PAY_STATUS : data.status == 3 ? CANCEL_STATUS :'';
                result.push(data);
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listOrderByTable() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listFoodOrders(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT * FROM order_foods 
            INNER JOIN orders ON orders.order_id = order_foods.order_id
            INNER JOIN foods ON foods.food_id = order_foods.food_id
            
            ORDER BY order_food_id DESC`;
            const resultSet = await DBApi.query(sql)
            
            let result = [];
            for await (const data of resultSet){
                sql = `SELECT order_food_materials.*,materials.* FROM order_food_materials
                INNER JOIN food_materials ON food_materials.food_material_id = order_food_materials.food_material_id
                INNER JOIN materials ON materials.material_id = food_materials.material_id
                WHERE order_food_materials.order_food_id=?`
                let queryParams = (data.order_food_id);
                data.food_materials = await DBApi.query(sql,queryParams)
                result.push(data);
            }
           

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listFoodOrders() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addOrder(orderCode,tableId,memberId,foods){
        const DBApi = new DBAPI();
        try {

            let sql = `INSERT INTO orders (order_code,table_id,member_id) VALUES(?,?,?)`;
            let queryParams = [orderCode,tableId,memberId];
            const resultSet1 = await DBApi.query(sql,queryParams)
            let data = {
                msg: MSG.FAILURE_INSERT_MESSAGE,
                isInsert: false,
                
            };
            if(resultSet1){
                const orderId = resultSet1.insertId;
                
                
                for(let i=0;i<foods.length;i++){
                    queryParams = [];
                    sql = `INSERT INTO order_foods (food_id,order_num,order_amount,order_id) VALUES(?,?,?,?)`;
                    queryParams.push(foods[i].food_id,foods[i].num,foods[i].amount,orderId);
                    const resultSet2 = await DBApi.query(sql,queryParams);
                    if(resultSet2){
                        queryParams = [];
                        const id = resultSet2.insertId;
                        sql = `INSERT INTO order_food_materials (food_material_id,food_material_price,order_food_id) VALUES`;
                        foods[i].food_materials.forEach((item,index)=>{
                            if(index != 0){
                                sql += ',';
                            }
                            sql += '(?,?,?)';
                            queryParams.push(item.food_material_id,item.food_material_price,id);
                        });
                        const resultSet3 = await DBApi.query(sql,queryParams);
                    }
                };
                
                

                data = {
                    msg: MSG.SUCCESS_INSERT_MESSAGE,
                    isInsert: true,
                    foodInsert : true,
                    id: orderId
                };

                
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.addOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editOrder(orderId,orderCode,tableId,memberId,status,foods){
        const DBApi = new DBAPI();
        try {
            let statusNum= '';
            switch(status){
                case COMING_STATUS :
                    statusNum = COMING_STATUS_NUM;
                    break;
                case NOT_PAY_STATUS :
                    statusNum = NOT_PAY_STATUS_NUM;
                    break;
                case PAY_STATUS :
                    statusNum = PAY_STATUS_NUM;
                    break;
                case CANCEL_STATUS :
                    statusNum = CANCEL_STATUS_NUM;
                    break;
            }
            let sql = `UPDATE orders SET order_code=?,table_id=?,member_id=?,status=? WHERE order_id=?`;
            let queryParams = [orderCode,tableId,memberId,statusNum,orderId];
            const resultSet1 = await DBApi.query(sql,queryParams)
            let data = {
                msg: MSG.FAILURE_UPDATE_MESSAGE,
                isUpdate: false,
                
            };
            if(resultSet1){
                data.id = orderId;
                data.msg = MSG.SUCCESS_UPDATE_MESSAGE
                data.isUpdate = true;
                data.isUpdate_order_foods = []
                queryParams = [];
                for(const food of foods){
                    sql = `UPDATE order_foods SET food_material_id=?,order_num=?,order_amount=?,order_id=? WHERE order_food_id=?`;
                    queryParams.push(food.food_material_id,food.num,food.amount,orderId,food.order_food_id);
                    const resultSet2 = await DBApi.query(sql,queryParams);
                    const temp = {
                        order_food_id: food.order_food_id,
                        isUpdate: false
                    };
                    if(resultSet2){
                        temp.isUpdate = true;
                    }
                    data.isUpdate_order_foods.push(temp);
                }
                
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.editOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editOrderStatus(orderId,status,pay){
        const DBApi = new DBAPI();
        try {
            let statusNum= '';
            switch(status){
                case COMING_STATUS :
                    statusNum = COMING_STATUS_NUM;
                    break;
                case NOT_PAY_STATUS :
                    statusNum = NOT_PAY_STATUS_NUM;
                    break;
                case PAY_STATUS :
                    statusNum = PAY_STATUS_NUM;
                    break;
                case CANCEL_STATUS :
                    statusNum = CANCEL_STATUS_NUM;
                    break;
            }

            let sql = `UPDATE orders SET status=? WHERE order_id=?`;
            let queryParams = [statusNum,orderId];

            if(pay){
                sql = `UPDATE orders SET status=?,pay=? WHERE order_id=?`;
                queryParams = [statusNum,pay,orderId];
            }

            const resultSet1 = await DBApi.query(sql,queryParams)
            console.log(resultSet1);
            let data = {
                msg: MSG.FAILURE_UPDATE_MESSAGE,
                isUpdate: false,
                
            };
            if(resultSet1){
                data.id = orderId;
                data.msg = MSG.SUCCESS_UPDATE_MESSAGE
                data.isUpdate = true;

            }
            
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.editOrderStatus() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editFoodOrder(orderFoodId,status){
        const DBApi = new DBAPI();
        try {
            let sql = `UPDATE order_foods SET order_status=? WHERE order_food_id=?`;
            let queryParams = [status,orderFoodId];
            const resultSet1 = await DBApi.query(sql,queryParams)
            let data = {
                msg: MSG.FAILURE_UPDATE_MESSAGE,
                isUpdate: false,
                
            };
            if(resultSet1){
                data.isUpdate = true;
                data.msg = MSG.SUCCESS_UPDATE_MESSAGE
                data.id = orderFoodId;
                
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.editFoodOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteOrder(orderId){
        const DBApi = new DBAPI();
        try {
            let sql = `DELETE FROM orders WHERE order_id=?`;
            let queryParams = [orderId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {
                msg:MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true
            };
            if(!resultSet){
                result = {
                    msg:MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false
                };
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.deleteOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }
    
}

module.exports = new OrderService();