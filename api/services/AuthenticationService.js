const DBAPI = require("../Api/DBApi");
const TransformModelUtils = require("../utils/TransformModelUtils");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");

class AuthenticationService{
    async login(username,password){
        const DBApi = new DBAPI();
        try {
            const sql = `SELECT MEMBER.member_id,MEMBER.member_firstname,MEMBER.member_lastname,MEMBER.member_username,MEMBER.member_img,ROLE.member_role_id,ROLE.member_role_title
            FROM members MEMBER
            INNER JOIN member_roles ROLE ON ROLE.member_role_id = MEMBER.member_role_id
            WHERE MEMBER.member_username=? AND MEMBER.member_password=?`;
            let queryParams = [];
            queryParams.push(username,password);
            const resultSet = await DBApi.query(sql,queryParams);
            console.log('QUERY DATA ---> ',resultSet);
            if(resultSet.length == 0){
                throw new ClientError(HTTP.CLIENT_ERROR_UNAUTHENTICATION_CODE,HTTP.CLIENT_ERROR_UNAUTHENTICATION_CODE,MSG.CLIENT_ERROR_UNAUTHENTICATION_MSG)
            }

            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
            
        } catch (error) {
            console.error("AuthenticationService.login() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        } finally{
            DBApi.closeConnection();
        }
    }
}

module.exports = new AuthenticationService();