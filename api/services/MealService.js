const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");
const fs = require('fs');
const { COMING_STATUS, NOT_PAY_STATUS, PAY_STATUS } = require("../constants/status");

class MealService{
    async listMeals(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT * FROM materials`;
            const resultSet = await DBApi.query(sql)
            for await (const data of resultSet){
                data.isEnable = data.isEnable ? true : false;
            }
            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
        } catch (error) {
            console.error("OrderService.listOrders() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listMealById(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT * FROM materials WHERE material_id=?`;
            let queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {};
            for await (const data of resultSet){
                data.isEnable = data.isEnable ? true : false;
                result = data;
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listOrderById() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listMealNotInFoodId(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT * FROM materials WHERE material_id NOT IN (SELECT material_id FROM food_materials WHERE food_id=?)`;
            let queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            console.log('RESULT ----> ',resultSet);
            
            let result = [];
            for await (const data of resultSet){
                data.isEnable = data.isEnable ? true : false;
                result = [...result,data];
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listOrderById() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addMeal(materialName){
        const DBApi = new DBAPI();
        try {
            let sql = `INSERT INTO materials (material_name) VALUES(?)`;
            let queryParams = [materialName];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_INSERT_MESSAGE,
                isInsert: true,
                id: resultSet.insertId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_INSERT_MESSAGE,
                    isInsert: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.addMeal() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editMeal(materialId,materialName,isEnable){
        const DBApi = new DBAPI();
        try {

            let sql = `UPDATE materials SET material_name=?,isEnable=? WHERE material_id=?`;
            let queryParams = [materialName,isEnable,materialId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isEdit: true,
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isEdit: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.editOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteMeal(materialId){
        const DBApi = new DBAPI();
        try {
            let sql = `DELETE FROM materials WHERE material_id=?`;
            let queryParams = [materialId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {
                msg:MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true
            };
            if(!resultSet){
                result = {
                    msg:MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false
                };
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.deleteOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addMealFood(body){
        const DBApi = new DBAPI();
        console.log('DATA----->',body);
        try {
            let sql = `INSERT INTO food_materials (material_id,food_id,food_material_price,isDefault) VALUES`;
            let queryParams = [];

            await body.forEach((el,i)=>{
                console.log(`LOOP[${i+1}]------>`,el);
                if(i>0){
                    sql += `,`;
                }
                sql += `(?,?,?,?)`;
                queryParams.push(el.material_id,el.food_id,el.food_material_price,el.isDefault);
                i++;
            });
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_INSERT_MESSAGE,
                isInsert: true,
                id: resultSet.insertId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_INSERT_MESSAGE,
                    isInsert: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.addMealFood() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async updateMealFood(id,price){
        const DBApi = new DBAPI();
        try {
            let sql = `UPDATE food_materials SET food_material_price=? WHERE food_material_id=?`;
            let queryParams = [price,id];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isUpdate: true,
                id: id
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isUpdate: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.updateMealFood() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteFoodMaterial(id){
        const DBApi = new DBAPI();
        console.log('DATA----->',id);
        try {
            let sql = `DELETE FROM food_materials WHERE food_material_id=?`;
            let queryParams = [id];            
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true,
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.deleteFoodMaterial() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    
}

module.exports = new MealService();