const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");

class TableService{
    async listTables(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT table_id,table_no,isShow FROM tables`;
            const resultSet = await DBApi.query(sql)
            for await (const data of resultSet){
                data.isShow = data.isShow ? true : false;
            }
            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
        } catch (error) {
            console.error("TableService.listTables() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listTableById(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT table_id,table_no,isShow FROM tables WHERE table_id=?`;
            const queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            let result = {};
            if(resultSet.length > 0){
                result = resultSet[0];
            }
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("TableService.listTables() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addTable(tableNo){
        const DBApi = new DBAPI();
        try {
            let sql = `INSERT INTO tables (table_no) VALUES(?)`;
            const queryParams = [tableNo];
            const resultSet = await DBApi.query(sql,queryParams)
            let result = {};
            if(resultSet){
                result = {
                    msg:MSG.SUCCESS_INSERT_MESSAGE,
                    isInsert: true,
                    id: resultSet.insertId
                }
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("TableService.addTable() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editTable(tableId,tableNo,isShow){
        const DBApi = new DBAPI();
        try {
            let sql = `UPDATE tables SET table_no=?,isShow=? WHERE table_id=?`;
            const queryParams = [tableNo,isShow,tableId];
            const resultSet = await DBApi.query(sql,queryParams)
            let result = {};
            if(resultSet){
                result = {
                    msg:MSG.SUCCESS_UPDATE_MESSAGE,
                    isEdit: true
                }
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("TableService.editTable() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteTable(tableId){
        const DBApi = new DBAPI();
        try {
            let sql = `DELETE FROM tables WHERE table_id=?`;
            const queryParams = [tableId];
            const resultSet = await DBApi.query(sql,queryParams)
            let result = {};
            if(resultSet){
                result = {
                    msg:MSG.SUCCESS_DELETE_MESSAGE,
                    isDelete: true
                }
            }
            
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("TableService.deleteTable() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }
}

module.exports = new TableService();