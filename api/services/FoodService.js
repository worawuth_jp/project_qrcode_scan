const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");
const fs = require('fs');

class FoodService{
    async listFoods(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT FOOD.food_id,FOOD.food_name,FOOD.food_price,FOOD.food_detail,FOOD.food_img,isEnable 
            FROM foods FOOD`;
            const resultSet = await DBApi.query(sql)
            for await (const data of resultSet){
                let query = `SELECT MATERIAL.material_name,FOOD_MATERIAL.food_material_id,FOOD_MATERIAL.food_material_price,FOOD_MATERIAL.isDefault
                FROM food_materials FOOD_MATERIAL
                INNER JOIN materials MATERIAL ON MATERIAL.material_id = FOOD_MATERIAL.material_id WHERE FOOD_MATERIAL.food_id=?`
                let params = [data.food_id];
                data.materials = await DBApi.query(query,params);
                data.isEnable = data.isEnable ? true : false;
            }
            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
        } catch (error) {
            console.error("FoodServices.listFoods() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listFoodById(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT food_id,food_name,food_price,food_detail,food_img,isEnable FROM foods WHERE food_id=?`;
            let queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {};
            for await (const data of resultSet){
                let query = `SELECT MATERIAL.material_name,FOOD_MATERIAL.food_material_id,FOOD_MATERIAL.food_material_price,FOOD_MATERIAL.isDefault
                FROM food_materials FOOD_MATERIAL
                INNER JOIN materials MATERIAL ON MATERIAL.material_id = FOOD_MATERIAL.material_id WHERE FOOD_MATERIAL.food_id=?`
                let params = [data.food_id];
                data.materials = await DBApi.query(query,params);
                data.isEnable = data.isEnable ? true : false;
                result = data;
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("FoodServices.listFoodById() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addFood(foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath){
        const DBApi = new DBAPI();
        try {

            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            let sql = `INSERT INTO foods (food_name,food_price,food_detail,food_img) VALUES(?,?,?,?)`;
            let queryParams = [foodName,foodPrice,foodDetail,imgPath];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_INSERT_MESSAGE,
                isInsert: true,
                id: resultSet.insertId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_INSERT_MESSAGE,
                    isInsert: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("FoodServices.addFood() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editFood(foodId,foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath){
        const DBApi = new DBAPI();
        try {

            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            let sql = `UPDATE foods SET food_name=?,food_price=?,food_detail=? `;
            let queryParams = [foodName,foodPrice,foodDetail];
            if(imgPath){
                sql += ` ,food_img=?`;
                queryParams.push(imgPath);
            }

            sql += ` WHERE food_id=?`;
            queryParams.push(foodId);
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isEdit: true,
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isEdit: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("FoodServices.editFood() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteFood(foodId){
        const DBApi = new DBAPI();
        try {
            let sql = `DELETE FROM foods WHERE food_id=?`;
            let queryParams = [foodId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {
                msg:MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true
            };
            if(!resultSet){
                result = {
                    msg:MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false
                };
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("FoodServices.deleteFood() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }
    
}

module.exports = new FoodService();