const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");
const sgMail = require('@sendgrid/mail')
const { COMING_STATUS, NOT_PAY_STATUS, PAY_STATUS, CANCEL_STATUS, COMING_STATUS_NUM, NOT_PAY_STATUS_NUM, PAY_STATUS_NUM, CANCEL_STATUS_NUM } = require("../constants/status");
const { PAY_CASH, PAY_CASH_TH, PAY_BANK_TH } = require("../constants/pay");
const { EMAIL, API_KEY, DOMAIN } = require("../constants/emails");
const mailgun = require("mailgun-js");
const dayjs = require('dayjs');
const mg = mailgun({apiKey: API_KEY, domain: DOMAIN});

class ReportService{
    async listReportPay(startDate,endDate){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT orders.pay,COUNT(*) AS NUM,SUM(order_foods.order_amount*order_foods.order_num) AS TOTAL
            FROM orders 
            INNER JOIN tables ON tables.table_id = orders.table_id
            LEFT JOIN order_foods ON order_foods.order_id = orders.order_id
            WHERE orders.status = 2 AND DATE(orders.created_at) BETWEEN ? AND ?
            GROUP BY orders.pay`;
            let queryParams = [startDate,endDate]
            const resultSet = await DBApi.query(sql,queryParams)
            for await (const data of resultSet){                
                data.pay_thai = data.pay == PAY_CASH ? PAY_CASH_TH : PAY_BANK_TH;
            }
            return resultSet;
        } catch (error) {
            console.error("ReportService.listReport() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listReportOrder(startDate,endDate){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT foods.*,COUNT(*) AS NUM,SUM(order_foods.order_amount*order_foods.order_num) AS TOTAL,orders.*
            FROM orders 
            LEFT JOIN order_foods ON order_foods.order_id = orders.order_id
            LEFT JOIN foods ON foods.food_id = order_foods.food_id
            WHERE orders.status = 2  AND DATE(orders.created_at) BETWEEN ? AND ?
            GROUP BY foods.food_id;`;
            let queryParams = [startDate,endDate]
            const resultSet = await DBApi.query(sql,queryParams)
            for await (const data of resultSet){                
                data.pay_thai = data.pay == PAY_CASH ? PAY_CASH_TH : PAY_BANK_TH;
            }
            return resultSet;
        } catch (error) {
            console.error("ReportService.listReport() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async sendEmail(startDate,endDate,email){
        try {
            console.log(email)
            let payStatusList = await this.listReportPay(startDate,endDate);
            let foodList = await this.listReportOrder(startDate,endDate);
            let html = `<h3>รายงานผลวันที่ ${dayjs(startDate).format('DD/MM/YYYY')} ถึง ${dayjs(endDate).format('DD/MM/YYYY')} </h3>`;
            let total = 0.00;
            html += "<br/><h3>ช่องทางการชำระ</h3>"

            html += '<table border="1">';
            html += '<thead><th scope="col">การชำระเงิน</th><th scope="col">จำนวน(ครั้ง)</th><th scope="col">รวม</th></thead>';
            html += '<tbody>';
            payStatusList.forEach((data)=>{
                total += data.TOTAL;
                html += `<tr><td>${data.pay_thai}</td><td>${data.NUM}</td><td>${data.TOTAL.toFixed(2)}</td></tr>`
            });
            html += '</tbody>';
            html += '</table>';

            html += "<br/><hr/><h3>รายการที่สั่ง</h3>"

            html += '<table border="1">';
            html += '<thead><th scope="col">รายการ</th><th scope="col">จำนวน</th><th scope="col">รวม</th></thead>';
            html += '<tbody>';
            foodList.forEach((data)=>{
                html += `<tr><td>${data.food_name}</td><td>${data.NUM}</td><td>${data.TOTAL.toFixed(2)}</td></tr>`
            });
            html += '</tbody>';
            html += '</table>';

            html += `<br/><hr/><br/><div style='color:darkblue;font-size: 22px'><span style="font-size:22px;">รายได้สุทธิ</span>${'    '}<span style="color:green;"> ${total}</span> บาท</div>`

            const data = {
                from: `Restaurant <no-reply@${DOMAIN}>`,
                to: email,
                subject: 'Report '+dayjs(startDate).format('DD/MM/YYYY') + ' TO '+dayjs(endDate).format('DD/MM/YYYY'),
                html: html
            };
            const result = await new Promise((resolve,reject)=>{
                mg.messages().send(data, function (error, body) {
                    resolve(body)
                });
            });
            
            //const result = await sgMail.send(msg);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("ReportService.sendEmail() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            
        }
    }
    
}

module.exports = new ReportService();