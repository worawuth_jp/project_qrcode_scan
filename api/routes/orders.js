const express = require("express");
const OrderEventRouter = require("./eventRouters/OrderEventRouter");

const router = express.Router();

router.get("/orders", (req, res) => {
  OrderEventRouter.listOrders(req, res);
});

router.post("/orders", (req, res) => {
  OrderEventRouter.addOrder(req, res);
});

router.put("/orders", (req, res) => {
  OrderEventRouter.editOrder(req, res);
});

router.put("/orders/status", (req, res) => {
  OrderEventRouter.editOrderStatus(req, res);
});

router.delete("/orders", (req, res) => {
  OrderEventRouter.deleteOrder(req, res);
});

router.get("/list-order-foods", (req, res) => {
  OrderEventRouter.listFoodOrder(req, res);
});

router.put("/order-foods", (req, res) => {
  OrderEventRouter.editFoodOrder(req, res);
});

module.exports = router;
