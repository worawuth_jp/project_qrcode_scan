const express = require("express");
const ReportEventRouter = require("./eventRouters/ReportEventRouter");

const router = express.Router();

router.get("/reports/pay", (req, res) => {
  ReportEventRouter.listReport(req,res)
});
router.get("/reports/orders", (req, res) => {
    ReportEventRouter.listReportOrder(req,res)
  });

  router.post("/reports/send-email", (req, res) => {
    ReportEventRouter.sendEmail(req,res)
  });

module.exports = router;
