const express = require("express");
const mealEventRouter = require("./eventRouters/mealEventRouter");

const router = express.Router();

router.get("/meals", (req, res) => {
  mealEventRouter.listMeals(req, res);
});

router.post("/meals", (req, res) => {
  mealEventRouter.addMeals(req, res);
});

router.put("/meals", (req, res) => {
  mealEventRouter.editMeals(req, res);
});

router.delete("/meals", (req, res) => {
  mealEventRouter.deleteMeals(req, res);
});

router.post("/add-meal-foods", (req, res) => {
  mealEventRouter.addMealFood(req, res);
});

router.put("/meal-foods", (req, res) => {
  mealEventRouter.updateMealFood(req, res);
});
router.delete("/meal-foods", (req, res) => {
  mealEventRouter.deleteMealFood(req, res);
});
module.exports = router;
