const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const ReportController = require("../../controllers/ReportController");

class ReportEventRouter{
    async listReport(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            
            successWithResult = await ReportController.listReportPay(body);
         
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('ReportEventRouter.listReport() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async listReportOrder(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            
            successWithResult = await ReportController.listReportOrder(body);
         
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('ReportEventRouter.listReportOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async sendEmail(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            let successWithResult ;
            
            successWithResult = await ReportController.sendEmail(body);
         
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('ReportEventRouter.sendEmail() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

}

module.exports = new ReportEventRouter();