const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const AuthController = require("../../controllers/AuthController");

class AuthEventRouter{
    async authentication(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            const successWithResult = await AuthController.authentication(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('AuthEventRouter.authentication() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }
}

module.exports = new AuthEventRouter();