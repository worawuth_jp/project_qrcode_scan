const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const UserController = require("../../controllers/UserController");

class UserEventRouter{
    async listUsers(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            const successWithResult = await UserController.listUsers(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('UserEventRouter.listUsers() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addUser(req,res){
        try {
            console.log('ParamsRequest --> ',req.body,req.file);
            const file = req.file;
            const body = req.body;
            const successWithResult = await UserController.addUser(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('UserEventRouter.addUser() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editUser(req,res){
        try {
            console.log('ParamsRequest --> ',req.body,req.file);
            const file = req.file;
            const body = req.body;
            const successWithResult = await UserController.editUser(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('UserEventRouter.editUser() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteUser(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            const successWithResult = await UserController.deleteUser(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('UserEventRouter.deleteUser() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }
}

module.exports = new UserEventRouter();