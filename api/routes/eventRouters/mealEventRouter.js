const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const MealController = require("../../controllers/MealController");

class MealEventRouter {
  async listMeals(req, res) {
    try {
      console.log("ParamsRequest --> ", req.query);
      const body = req.query;
      let successWithResult;
      if (body.meal_id || body.not_in) {
        successWithResult = await MealController.listMealsById(body);
      } else {
        successWithResult = await MealController.listMeals();
      }
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.listMeals() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async addMeals(req, res) {
    try {
      console.log("ParamsRequest --> ", req.body);
      const body = req.body;
      let successWithResult = await MealController.addMeal(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.addMeals() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async editMeals(req, res) {
    try {
      console.log("ParamsRequest --> ", req.body);
      const body = req.body;
      const file = req.file;
      let successWithResult = await MealController.editMeal(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.editMeals() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async deleteMeals(req, res) {
    try {
      console.log("ParamsRequest --> ", req.query);
      const body = req.query;
      let successWithResult = await MealController.deleteMeal(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.deleteMeals() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async addMealFood(req, res) {
    try {
      console.log("ParamsRequest --> ", req.body);
      const body = req.body;
      let successWithResult = await MealController.addMealFood(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.addMeals() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async updateMealFood(req, res) {
    try {
      console.log("ParamsRequest --> ", req.body);
      const body = req.body;
      let successWithResult = await MealController.updateMealFood(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.updateMealFood() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }

  async deleteMealFood(req, res) {
    try {
      console.log("ParamsRequest --> ", req.body);
      const body = req.query;
      let successWithResult = await MealController.deleteMealFood(body);
      res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
    } catch (error) {
      console.error("MealEventRouter.deleteMealFood() Exception ---> ", error);
      if (error.code != void 0) {
        res.status(error.http).send({
          statusCode: error.code,
          statusMessage: error.data.message,
        });
      } else {
        res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
          statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
          statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
        });
      }
    }
  }
}

module.exports = new MealEventRouter();
