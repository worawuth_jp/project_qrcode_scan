const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const OrderController = require("../../controllers/OrderController");

class OrderEventRouter{
    async listOrders(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            if(body.order_id || body.table_id){
                successWithResult = await OrderController.listOrderById(body);
            }
            else{
                successWithResult = await OrderController.listOrders();
            }
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.listOrders() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async listFoodOrder(req,res){
        try {
            
            let successWithResult ;
           
            successWithResult = await OrderController.listFoodOrders();
            
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.listFoodOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addOrder(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            const file = req.file;
            let successWithResult = await OrderController.addOrder(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.addOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editOrder(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            const file = req.file;
            let successWithResult = await OrderController.editOrder(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.editOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editOrderStatus(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            let successWithResult = await OrderController.editOrderStatus(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.editOrderStatus() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editFoodOrder(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            let successWithResult = await OrderController.editOrderFood(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.editOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteOrder(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult = await OrderController.deleteOrder(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('OrderEventRouter.deleteOrder() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

}

module.exports = new OrderEventRouter();