const HTTP = require("../constants/http");
const MSG = require("../constants/message");

class ClientError extends Error{
    constructor(http,code,msg,stack){
        super(http,code,msg,stack);
        this.http = http;
        this.code = code;
        this.msg = msg;
        this.stack = stack;
        this.error = this.generateClientError(this.http,this.code,this.msg,this.stack);
        this.responseCode = this.error.responseCode;
        this.data = this.error.data;
    }

    generateClientError(http,code,msg,stack){
        let error = {};
        switch(http){
            case HTTP.CLIENT_ERROR_BAD_REQUEST_CODE :
                error = {
                    responseCode: HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,
                    data: {
                        message: msg ? msg : MSG.CLIENT_ERROR_BAD_REQUEST_MSG
                    },
                    code: code ? code : HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,
                    mesage: MSG.CLIENT_ERROR_BAD_REQUEST_MSG,
                    stack: stack
                }
                break;
            case HTTP.CLIENT_ERROR_NOT_FOUND_CODE :
                error = {
                    responseCode : HTTP.CLIENT_ERROR_NOT_FOUND_CODE,
                    data : {
                        message: msg ? msg : MSG.CLIENT_ERROR_NOT_FOUND_MSG
                    },
                    code: code ? code : HTTP.CLIENT_ERROR_NOT_FOUND_CODE,
                    mesage: MSG.CLIENT_ERROR_NOT_FOUND_MSG,
                    stack: stack
                }
                break;
            default :
                error = {
                    responseCode : HTTP.CLIENT_ERROR_UNAUTHENTICATION_CODE,
                    data : {
                        message: msg ? msg: MSG.CLIENT_ERROR_UNAUTHENTICATION_MSG
                    },
                    code: code ? code : HTTP.CLIENT_ERROR_UNAUTHENTICATION_CODE,
                    mesage: MSG.CLIENT_ERROR_UNAUTHENTICATION_MSG,
                    stack: stack
                }
        }
        return error;
    }
}

module.exports = ClientError;