const NETWORK_IP = '192.168.1.36';
const HOST = `http://${NETWORK_IP}:5000`;
const BASE_API_PATH = `${HOST}/api`;

export {HOST,BASE_API_PATH};
